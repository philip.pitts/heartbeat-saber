# Heartbeat Saber

Definitely not a rip-off of any popular VR games!

![](docs/HeartbeatSaberDemo.mp4)

## Headset Support

This project is built for Windows MxR headsets and may have dependencies that prevent other headsets from working correctly. Generic Unity [Input Actions](https://docs.unity3d.com/Packages/com.unity.inputsystem@1.3/manual/Actions.html) have been used as much as possible.

Ensure that support for your headset is enabled via the `Edit` -> `Project Settings` -> `XR Plugin Management` plugin.

![](docs/XRPluginManagement.png)

Review the [Input Action Maps](https://docs.unity3d.com/Packages/com.unity.xr.interaction.toolkit@1.0/manual/index.html#interaction-manager) and ensure they are compatiable with your controllers.

![](docs/UIActionMap.png)

![](docs/XRToolkitActionMaps.png)

## Music Files

Music files have not been uploaded to this repository for legal reasons. You will need to provide your own audio files and add them to a level [Timeline](https://docs.unity3d.com/Packages/com.unity.timeline@1.2/manual/tl_window.html).

## Creating Levels

Choreography for the opening bars of song have been implemented as an example level. Note that music files have not been included in this repository for legal reasons and must be purchased and added after cloning.

![](docs/ChoreographaphyExample.png)

Use the clips in `Helpers` to build out the choreography of your levels. There are four groups of helper clips that correspond to four spawn point groups:

* Outside Right
* Inside Right
* Inside Left
* Outside Left

Each spawn point has several orientations that control how a spawned cube must be sliced:

* Up
* Down
* Left
* Right
* Diagonal Up Right
* Diagonal Up Left
* Diagonal Down Right
* Diagonal Down Left

As you create your choreography, duplicate the required clips and drag them into a track group for the level.

![](docs/HelperClips.png)

During development you should move the audio track to the beginning of the Timeline. Sync the start of each clip in the choreography to the place on the audio track where it should be sliced. Note that Unity's audio graph visual is not always correct, and it may be necessary to calculate clip start times using the song's BPM. Unity Timelines will play back at 60 frames per second.

When you have finished the choreography or want to test the level, move the audio track so that its start aligns with the end of the `Marker` clip. This will ensure the first cube arrives at the correct time. Ensure your audio track is clipped so that there is no dead space at the beginning of the track.

![](docs/AlignAudioClip.png)

Once completed, load your level by assigning it to the `Playable` slot on the `Playable Director` component of the `Game Manager`.

![](docs/PlayableSlot.png)

## Acknowledgements

Special thanks to [jayfadden2](https://www.cgtrader.com/jayfadden2) at CGTrader for making their [lightsaber model](https://www.cgtrader.com/free-3d-models/military/melee/luke-skywalkers-lightsaber) open to the public. The model is used for the sabers in this game.

The game also makes use the Unity [VFX Graph](https://unity.com/visual-effect-graph) example assets for the sparks and the Unity [XR Toolkit](https://docs.unity3d.com/Packages/com.unity.xr.interaction.toolkit@2.0/manual/index.html) example Input Action Maps for controller input.
