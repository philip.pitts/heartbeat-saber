﻿using System;
using UnityEngine;
using UnityEngine.InputSystem;

namespace HeartbeatSaber
{
    public class XRControllerManager : MonoBehaviour
    {
        public Action MenuPressed;
    
        public GameObject DefaultInteractor;
        public GameObject RayInteractor;

        private void Awake()
        {
            DefaultInteractor.GetComponent<PlayerInput>().onActionTriggered += HandleAction;
        }

        private void HandleAction(InputAction.CallbackContext context)
        {
            if (context.action.name == "Menu") MenuPressed?.Invoke();
        }
    }
}