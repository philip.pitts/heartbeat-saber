﻿using System.Collections;
using UnityEngine;
using UnityEngine.VFX;

namespace HeartbeatSaber
{
    public class SparkController : MonoBehaviour
    {
        [SerializeField] private float ActiveTime = 0.25f;
        [SerializeField] private float InactiveTime = 1f;
        
        private VisualEffect vfx;

        private void Awake()
        {
            vfx = GetComponent<VisualEffect>();
        }

        private IEnumerator Start()
        {
            yield return new WaitForSeconds(ActiveTime);
            vfx.Stop();
            yield return new WaitForSeconds(InactiveTime);
            Destroy(gameObject);
        }
    }
}