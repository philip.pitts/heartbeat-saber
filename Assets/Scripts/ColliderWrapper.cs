using System;
using UnityEngine;

namespace HeartbeatSaber
{
    [RequireComponent(typeof(Collider))]
    public class ColliderWrapper : MonoBehaviour
    {
        public Action<Collider> Triggered;
        [HideInInspector] public bool IsColliding;

        private void OnTriggerEnter(Collider other)
        {
            IsColliding = true;
            Triggered?.Invoke(other);
        }

        private void OnTriggerExit(Collider other)
        {
            IsColliding = false;
            Triggered?.Invoke(other);
        }
    }
}