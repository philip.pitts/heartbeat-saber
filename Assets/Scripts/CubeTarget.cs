using System.Collections;
using UnityEngine;

namespace HeartbeatSaber
{
    public class CubeTarget : MonoBehaviour
    {
        [Tooltip("The length of time in seconds until the cube hits the back wall")]
        [SerializeField] private float TravelTime = 10f;
        [Tooltip("The distance from the spawn point to the back wall")]
        [SerializeField] private float TravelDistance = 120f;
        
        [SerializeField] private GameObject HalfCubePrefab;
        [SerializeField] private GameObject SparksPrefab;

        [SerializeField] private ColliderWrapper Top;
        [SerializeField] private ColliderWrapper Bottom;
        [SerializeField] private ColliderWrapper FullCube;

        private GameManager gameManager;

        private bool isTopActivated;
        private bool isBottomActivated;

        private void Awake()
        {
            gameManager = FindObjectOfType<GameManager>();
        }

        private void Start()
        {
            FullCube.Triggered += OnCubeTriggered;
            StartCoroutine(MoveForward());
        }
        
        private IEnumerator MoveForward()
        {
            var elapsed = 0f;
            var position = transform.localPosition;
            while (elapsed < TravelTime)
            {
                yield return null;
                if (gameManager.IsPaused) continue;
                
                position.z = -Mathf.Lerp(0, TravelDistance, elapsed / TravelTime);
                transform.localPosition = position;
                elapsed += Time.deltaTime;
            }
            
            gameManager.RegisterMiss();
            Destroy(gameObject);
        }

        private void OnCubeTriggered(Collider other)
        {
            if (Bottom.IsColliding) isBottomActivated = true;
            if (Top.IsColliding && isBottomActivated) isTopActivated = true;
            if (!FullCube.IsColliding && !(isBottomActivated && isTopActivated)) ResetActivations();
            if (!FullCube.IsColliding && isBottomActivated && isTopActivated) Slice();
        }

        private void ResetActivations()
        {
            isTopActivated = false;
            isBottomActivated = false;
        }

        private void Slice()
        {
            ResetActivations();
            
            var position = transform.position;
            var rotation = transform.rotation;
            
            Instantiate(SparksPrefab, position, rotation);
            Instantiate(HalfCubePrefab, position, rotation);
            Instantiate(HalfCubePrefab, position + Vector3.forward, rotation * Quaternion.Euler(0, 180, 0));

            gameManager.RegisterSlice();
        
            Destroy(gameObject);
        }
    }
}