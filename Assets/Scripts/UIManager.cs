using TMPro;
using UnityEngine;

namespace HeartbeatSaber
{
    public class UIManager : MonoBehaviour
    {
        [SerializeField] private TMP_Text ScoreMultiplierText;
        [SerializeField] private TMP_Text ComboCountText;
        [SerializeField] private TMP_Text ScoreText;
        [SerializeField] private TMP_Text StartText;

        [SerializeField] private GameObject CenterPanel;
        [SerializeField] private GameObject RightPanel;
        [SerializeField] private GameObject LeftPanel;
        [SerializeField] private GameObject ResumeButton;

        private GameManager gameManager;
        
        public int ScoreMultiplier { set => ScoreMultiplierText.text = value.ToString(); }
        public int ComboCount { set => ComboCountText.text = value.ToString(); }
        public int Score { set => ScoreText.text = value.ToString(); }
        
        public void ResetMenu() => Start();
        
        public void ShowMainMenu() => CenterPanel.SetActive(true);

        private void Awake()
        {
            gameManager = FindObjectOfType<GameManager>();
        }

        private void Start()
        {
            ResumeButton.SetActive(false);
            RightPanel.SetActive(false);
            LeftPanel.SetActive(false);
            StartText.text = "Start";
        }

        public void StartGame()
        {
            RightPanel.SetActive(true);
            LeftPanel.SetActive(true);
            
            ResumeButton.SetActive(true);
            CenterPanel.SetActive(false);
            StartText.text = "Restart";

            gameManager.StartGame();
        }

        public void ResumeGame()
        {
            CenterPanel.SetActive(false);
            gameManager.ResumeGame();
        }

        public void QuitGame()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
            #else
            Application.Quit();
            #endif
        }
    }
}