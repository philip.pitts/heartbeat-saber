﻿using UnityEngine;
using UnityEngine.Playables;

namespace HeartbeatSaber
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private int MaxScoreMultiplier = 8;
        
        [SerializeField] private UIManager UI;
        [SerializeField] private XRControllerManager ControllerRH;
        [SerializeField] private XRControllerManager ControllerLH;

        private int scoreMultiplier;
        private int comboCount;
        private int score;

        private bool isStarted;
        private bool isPaused;
        private bool isResetting;

        private PlayableDirector playableDirector;

        public bool IsPaused => isPaused;

        private void Awake()
        {
            playableDirector = GetComponent<PlayableDirector>();
            playableDirector.stopped += OnPlayableDirectorStopped;
            ControllerLH.MenuPressed += OnMenuPressed;
            ControllerRH.MenuPressed += OnMenuPressed;
        }

        public void Start()
        {
            ToggleUIMode(true);
        }

        public void StartGame()
        {
            score = 0;
            comboCount = 0;
            scoreMultiplier = 0;

            isStarted = true;
            isPaused = false;
            
            UpdateStats();
            ToggleUIMode(false);
            RestartTimeline();
        }

        public void ResumeGame()
        {
            isPaused = false;
            ToggleUIMode(false);
            playableDirector.Resume();
        }

        public void RegisterSlice()
        {
            scoreMultiplier++;
            if (scoreMultiplier > MaxScoreMultiplier) scoreMultiplier = MaxScoreMultiplier;

            comboCount++;
            score += scoreMultiplier;
            
            UpdateStats();
        }

        public void RegisterMiss()
        {
            scoreMultiplier = 0;
            comboCount = 0;
            UpdateStats();
        }

        private void UpdateStats()
        {
            UI.ScoreMultiplier = scoreMultiplier;
            UI.ComboCount = comboCount;
            UI.Score = score;
        }

        private void ToggleUIMode(bool active)
        {
            ControllerLH.DefaultInteractor.SetActive(!active);
            ControllerRH.DefaultInteractor.SetActive(!active);
            ControllerRH.RayInteractor.SetActive(active);
        }

        private void OnMenuPressed()
        {
            if (!isStarted || isPaused) return;

            isPaused = true;
            ToggleUIMode(true);
            playableDirector.Pause();
            UI.ShowMainMenu();
        }

        private void RestartTimeline()
        {
            isResetting = true;
            playableDirector.Stop();
            foreach (var cube in FindObjectsOfType<CubeTarget>()) Destroy(cube.gameObject);
            
            playableDirector.time = 0;
            playableDirector.Play();
        }
        
        private void OnPlayableDirectorStopped(PlayableDirector director)
        {
            // This can be called when exiting play mode if the playable director is running.
            // Check to make sure the UI hasn't been destroyed already.
            if (UI == null) return;
            
            // Make sure we didn't manually call Stop on the director to reset it.
            if (isResetting)
            {
                isResetting = false;
                return;
            }
            
            // The director has finished playing the full timeline
            isStarted = false;
            UI.ResetMenu();
            UI.ShowMainMenu();
        }
    }
}