﻿using System.Collections;
using UnityEngine;

namespace HeartbeatSaber
{
    public class SlicedCube : MonoBehaviour
    {
        [SerializeField] private float Lifetime = 1f;
        [SerializeField] private float FadeTime = 0.5f;

        private new Rigidbody rigidbody;
        private MeshRenderer meshRenderer;
        
        private void Awake()
        {
            rigidbody = GetComponentInChildren<Rigidbody>();
            meshRenderer = GetComponentInChildren<MeshRenderer>();
        }

        private void Start()
        {
            rigidbody.AddRelativeForce(Vector3.left, ForceMode.Impulse);
            rigidbody.AddRelativeTorque(Random.Range(5f, 20f), Random.Range(5f, 20f), Random.Range(20f, 40f));
            StartCoroutine(FadeAndDestroy());
        }

        private IEnumerator FadeAndDestroy()
        {
            yield return new WaitForSeconds(Lifetime);
            var color = meshRenderer.material.color;

            var elapsed = 0f;
            while (elapsed < FadeTime)
            {
                color.a = Mathf.Lerp(1f, 0f, elapsed / FadeTime);
                meshRenderer.material.color = color;

                yield return null;
                elapsed += Time.deltaTime;
            }

            Destroy(gameObject);
        }
    }
}